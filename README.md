# INFINITE GLITCH

- - -

## Abstract

An automatically generated, never-repeating, abstract glitch video constructed from the mass of new media uploaded to the web every day. Source audio, images, and video will be ripped daily from a variety of media hosting sites and then combined using collage and glitch techniques to create an organic, ever-changing stream of media. Only free and open source software will be used.

The stream will play 24/7 for 1 year on a public website.

- - -

## Functionality

Infinite Glitch provides a modular system for downloading source videos, transcoding them, combining them using guided-random frame reordering techniques, and streaming them.

Each element of the system can be used independant of the others, some through command line interfaces.


### collect

- allow for more focused search settings using archive.org json api
- log metadata for found media in database


### construct

- create final video constructs
- log them in database with reference to their source media


### curate

- select and return finished constructs for playback
- provide metadata for current (most recently selected) construct's source files


The system also provides the means to be automated using Beanstalk task handling and cron.

Source media metadata is logged in a database upon collection; tied to a related table of final videos by the constructor; provided as playlist metadata by the curator

- - -

## Getting Started

### Install RethinkDB

http://www.rethinkdb.com/docs/install/ubuntu/

`rethinkdb --daemon`


### Install Dependencies

#### Ubuntu 14.04

Add ffmpeg ppa: https://launchpad.net/~jon-severinsson/+archive/ubuntu/ffmpeg

`sudo apt-get install ffmpeg icecast2 ezstream`

- - -

## Execution

### Icecast

`rake start_icecast`

### Ezstream

`rake start_ezstream`

Note that config/ezstream.xml MUST be 644 permissions or it will not run.
