require 'spec_helper'

include TestDummyHelpers

describe Controller do


  before :all do
    module Controller

      include TestDummyHelpers

      def collect(num = 1)
        return generate_source(num)
      end

      def construct(num = 1)
        return generate_final(num)
      end

    end


    module FileHandler; public :all_matching_files; end
  end


  before :each do
    SourceFileHandler.empty_pool
    FinalFileHandler.empty_pool
    Archivist.clear_tables
  end


  it "should fill and empty source pool" do
    initial_count = SourceFileHandler.all_matching_files.length
    Controller.fill_source_pool
    filled_count = SourceFileHandler.all_matching_files.length
    Controller.empty_source_pool
    emptied_count = SourceFileHandler.all_matching_files.length

    initial_count.should_not == Settings.source_max
    filled_count.should == Settings.source_max
    emptied_count.should == 0
  end


  it "should fill and empty final pool" do
    generate_source(Settings.sources_per_final)

    initial_count = FinalFileHandler.all_matching_files.length
    Controller.fill_final_pool
    filled_count = FinalFileHandler.all_matching_files.length
    Controller.empty_final_pool
    emptied_count = FinalFileHandler.all_matching_files.length

    initial_count.should_not == Settings.final_max
    filled_count.should == Settings.final_max
    emptied_count.should == 0
  end


  it "should reduce the source pool if overfilled" do
    generate_source(Settings.source_max + 5)
    
    initial_count = SourceFileHandler.all_matching_files.length
    Controller.fill_source_pool
    filled_count = SourceFileHandler.all_matching_files.length
    Controller.empty_source_pool

    initial_count.should_not == Settings.source_max
    filled_count.should == Settings.source_max
  end


  it "should not fill final pool if fewer source files than min" do
    error = ''
    begin
      Controller.fill_final_pool
      filled = true
    rescue => e
      filled = false
      error = e.to_s
    end
    filled.should == false
    error.should == 'Cannot construct without necessary source files'
  end


  it "should only return ogv format files" do
    generate_final(3)
    Controller.next_video.should include Settings.final_format
  end


  it "should return a full file path for next file" do
    generate_final(3)
    Controller.next_video.match(/^\/.*/).should_not == nil
  end


  it "should return a single next file" do
    generate_final(3)
    Controller.next_video.class.should == String
  end


  it "should update the 'current' record when next_video is called" do
    generate_final(3)

    new_current_file = Controller.next_video
    new_current_id = Archivist.get_current_id

    new_current_file.should include new_current_id
  end


  it "should clean orphan final records and files" do
    legit_ids = generate_final(2)
    orphan_record_id = '12345'
    orphan_file_id = '67890'

    generate_final_records([orphan_record_id])
    generate_final_files([orphan_file_id])

    Controller.clean_final_pool

    all_record_ids = Archivist.get_all_final_ids
    all_file_ids = FinalFileHandler.all_matching_file_ids

    legit_ids.all? {|id| all_record_ids.include? id}.should == true
    legit_ids.all? {|id| all_file_ids.include? id}.should == true
    all_record_ids.include?(orphan_record_id).should == false
    all_file_ids.include?(orphan_file_id).should == false
  end


  it "should clean orphan source records and files" do
    legit_ids = generate_source(2)
    orphan_record_id = '12345'
    orphan_file_id = '67890'

    generate_source_records([orphan_record_id])
    generate_source_files([orphan_file_id])

    Controller.clean_source_pool

    all_record_ids = Archivist.get_all_source_ids
    all_file_ids = SourceFileHandler.all_matching_file_ids

    legit_ids.all? {|id| all_record_ids.include? id}.should == true
    legit_ids.all? {|id| all_file_ids.include? id}.should == true
    all_record_ids.include?(orphan_record_id).should == false
    all_file_ids.include?(orphan_file_id).should == false
  end


  # TODO
  it "should handle orphan records gracefully" do
    # curator should delete orphan records if file not found
    # and try again until pool empty or valid record found
  end


end
