require 'simplecov'
SimpleCov.start do 
  add_filter do |file|
    !file.filename.include?("/lib/")
  end
end

require 'securerandom'
require_relative '../lib/infiniteglitch'
include InfiniteGlitch

ROOT_DIR = "#{File.dirname(__FILE__)}/../"


RSpec.configure do |config|

  config.before :suite do
    spec_dir = "#{File.dirname(__FILE__)}/"
    InfiniteGlitch::Settings.const_set(:ROOT_DIR, spec_dir)
    InfiniteGlitch::Archivist.const_set(:DATABASE_NAME, 'testdb')
    InfiniteGlitch::Archivist.initialize_database
  end


  config.after :suite do
    InfiniteGlitch::Archivist.delete_database
  end

end


module TestDummyHelpers


  def create_test_directories
    Settings.locations.each do |k,v|
      dir = Settings.location(k)
      if !File.exist?(dir)
        system "mkdir -p #{dir}" 
        puts "created location '#{dir}'"
      end
    end
  end


  def generate_orig(num)
    fake_ids = (0...num).map {generate_id}
    generate_files('tmp', fake_ids)
  end


  def generate_source(num)
    fake_ids = (0...num).map {generate_id}
    generate_source_records(fake_ids)
    generate_source_files(fake_ids)
    return fake_ids
  end


  def generate_final(num)
    fake_ids = (0...num).map {generate_id}
    generate_final_records(fake_ids)
    generate_final_files(fake_ids)
    return fake_ids
  end


  def generate_current
    final_id = generate_final(1).first
    Archivist.update_current(final_id)
    return final_id
  end


  def generate_source_files(ids)
    return generate_files('source', ids)
  end


  def generate_final_files(ids)
    return generate_files('final', ids)
  end


  def generate_source_records(fake_ids)
    fake_ids.each do |id|
      test_source = {
        :id => id,
        :thing => 'test'
      }
      Archivist.create_source(test_source)
    end
    return fake_ids
  end


  def generate_final_records(fake_ids)
    fake_ids.each do |id|
      test_final = {
        :id => id,
        :sources => [
          {:id => generate_id, :thing => 'test'}
        ]
      }
      Archivist.create_final(test_final)
    end
    return fake_ids
  end


  private


  def generate_files(loc, fake_ids)
    fake_files = fake_ids.map {|id| generate_file(loc, id)}
    return fake_files
  end


  def generate_file(loc, id)
    dir = Settings.location(loc)
    sample = sample_media_file(loc)
    sample_format = sample.match(/\.([a-zA-Z]+)$/)[1]
    filename = "#{id}.#{sample_format}"
    system "ln -s '#{sample}' '#{dir}/#{filename}'"
    return filename
  end


  def sample_media_file(loc)
    sample = Dir.glob("#{ROOT_DIR}/spec/media/samples/#{loc}_sample*")
    if sample.length > 0
      return sample.first
    else
      raise "No sample media file found."
    end
  end


  def generate_id
    return SecureRandom.uuid.gsub("-", "")
  end


end
