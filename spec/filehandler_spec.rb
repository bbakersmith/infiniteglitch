require 'spec_helper'

include TestDummyHelpers

describe FileHandler do


  before :all do
    module FileHandler; public :all_matching_files; end
    create_test_directories
    @filehandler = FileHandler
    @final_filehandler = FinalFileHandler
    @source_filehandler = SourceFileHandler
    SOURCE_DIR = Settings.location('source')
    SOURCE_FORMAT = Settings.source_format
    FINAL_DIR = Settings.location('final')
    FINAL_FORMAT = Settings.final_format
  end  


  it "should delete all files in a pool" do
    fake_ids = generate_final(4)
    deleted_ids = @final_filehandler.empty_pool

    fake_ids.all? {|id| deleted_ids.include?(id)}.should == true
    Dir.glob("#{FINAL_DIR}/*#{FINAL_FORMAT}").should be_empty
  end
  

  it "should remove excess files from pool" do
    max_files = Settings.final_max
    extra_files = 4
    fake_ids = generate_final(max_files + extra_files)
    deleted_ids = @final_filehandler.remove_excess_from_pool
    deleted_ids.length.should >= extra_files
  end


  it "should get a file by id" do
    fake_id = generate_final(1).first
    @final_filehandler.get_file_by_id(fake_id).should include fake_id
  end


  it "should delete a file by id" do
    fake_id = generate_final(1).first
    @final_filehandler.get_file_by_id(fake_id).should_not == nil
    @final_filehandler.delete_file_by_id(fake_id).should == fake_id
    @final_filehandler.get_file_by_id(fake_id).should == nil
  end


  it "should remove all files from pool" do
    fake_ids = generate_final(4)
    
    @final_filehandler.all_matching_files.length.should > 0
    
    deleted_ids = @final_filehandler.empty_pool
    fake_ids.all? {|id| deleted_ids.include? id}.should == true

    @final_filehandler.all_matching_files.length.should == 0
  end


end
