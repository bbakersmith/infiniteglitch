require 'spec_helper'

describe Settings do


  before :all do
    module Settings
      attr_reader :_settings
    end
  end


  before :each do
    @settings = Settings
  end


  it "should deep merge without disturbing unchanged values" do
    unchanged_location = @settings.locations["final"]
    unchanged_max_source_size = @settings.max_source_size

    new_settings = {
      "locations" => {
        "source" => "not/your/source"
      },
      "database" => "nodb"
    }

    @settings.set(new_settings)

    @settings.locations["final"].should == unchanged_location
    @settings.max_source_size.should == unchanged_max_source_size
    @settings.locations["source"].should == "not/your/source"
    @settings.database.should == "nodb"
  end


end
