require 'spec_helper'

include TestDummyHelpers

describe Archivist do


  before :all do
    module Archivist
      public :is_current?, :delete_current, :get_all_ids
    end
    @archivist = Archivist
  end


  before :each do
    @test_source = {
      :id => 'ABCDE',
      :thing => 'test'
    }

    @test_final = {
      :id => '12345',
      :sources => [
        @test_source
      ]
    }

    @test_final_current = {
      :id => '67890',
      :sources => [
        @test_source
      ]
    }

    @test_current = {
      :final_id => @test_final_current[:id],
      :id => 1
    }
  end


  after :each do
    Archivist.clear_tables
  end


  it "should set metadata for a source with an id" do
    id = @archivist.create_source(@test_source)
    @archivist.get_source(id)["thing"].should == "test"
  end


  it "should set metadata for a source without an id" do
    test = {"thing" => "test"}
    id = @archivist.create_source(test)
    @archivist.get_source(id)["thing"].should == "test"
  end


  it "should set metadata for a final video" do
    id = @archivist.create_final(@test_final)
    @archivist.get_final(id)["sources"].first["thing"].should == "test"
  end


  it "should delete a final" do
    id = @archivist.create_final(@test_final)
    @archivist.delete_final(id).should == 1
  end


  it "should update the current final" do
    @archivist.update_current(@test_final_current[:id])
    @archivist.get_current_id.should == "67890"
  end


  it "should correctly identify the current final" do
    @archivist.update_current(@test_final_current[:id])
    @archivist.is_current?("67890").should == true
  end


  it "should delete current" do
    @archivist.update_current(@test_final_current[:id])
    @archivist.delete_current.should == 1
    @archivist.get_current_id.should be_false
  end


  it "should not delete the current final" do
    id = @archivist.create_final(@test_final_current)
    @archivist.update_current(@test_final_current[:id])
    @archivist.delete_final(id).should == 0
  end


  it "should force delete the final and current if final is current" do
    id = @archivist.create_final(@test_final_current)
    @archivist.update_current(@test_final_current[:id])
    @archivist.force_delete_final(id).should == 1
    @archivist.get_final(id).should be_false
    @archivist.get_current_id.should be_false
  end


  it "should get all the ids of the source table" do
    fake_ids = generate_source(3)
    source_ids = @archivist.get_all_source_ids
    all_present = fake_ids.all? {|id| source_ids.include?(id)}
    all_present.should == true
  end


  it "should get all the ids of the final table" do
    fake_ids = generate_final(3)
    final_ids = @archivist.get_all_final_ids
    all_present = fake_ids.all? {|id| final_ids.include?(id)}
    all_present.should == true
  end


end
