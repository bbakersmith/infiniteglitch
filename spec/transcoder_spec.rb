require 'spec_helper'

include TestDummyHelpers

describe Transcoder do


  module Transcoder
    public :generate_cmd

    def run_cmd(cmd)
      return cmd
    end
  end


  before :each do
    @transcoder = Transcoder
  end


  it "should generate a correct command" do
    cmd = @transcoder.generate_cmd('test1', 'test2')
    cmd.should include 'test1'
    cmd.should include 'test2' 
  end


  it "should transcode a file in tmp and delete the original" do
    orig_file = generate_orig(1).first
    new_file = SourceTranscoder.transcode(orig_file)
    File.exist?(orig_file).should == false
    new_file.should_not == orig_file
    new_file.should include orig_file
  end  


  it "should fail to transcode a file in that doesn't exist" do
    orig_file = 'iamnotreal.ogv'
    failed = false
    begin
      FinalTranscoder.transcode(orig_file)
    rescue => e
      failed = e.to_s
    end
    failed.should include "No such file or directory"
  end  


end
