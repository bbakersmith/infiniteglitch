require 'spec_helper'

include TestDummyHelpers

describe Constructor do


  before :all do
    class Constructor
      attr_reader :sources, :segments, :timeline

      public :load_source_videos, 
             :generate_slide_segments, 
             :initialize_timeline, 
             :generate_video
    end

    Settings.set({
      :min_final_size => 60,
      :max_segment_size => 59,
      :slides_per_final => 1
    })

    generate_source(10)
  end


  before :each do
    @constructor = Constructor.new
  end


  it "should load source videos" do
    before = @constructor.sources.length
    @constructor.load_source_videos
    after = @constructor.sources.length

    before.should == 0 
    after.should > 0
  end


  it "should generate slide segments" do
    @constructor.load_source_videos

    before = @constructor.segments[:slides].length
    @constructor.generate_slide_segments
    after = @constructor.segments[:slides].length

    before.should == 0 
    after.should > 0
  end


  it "should initialize the timeline with a keyframe" do
    @constructor.load_source_videos

    @constructor.initialize_timeline
    after = @constructor.timeline.size
    after.should == 1
  end


  it "should generate a long enough video" do
    @constructor.load_source_videos
    @constructor.generate_slide_segments
    @constructor.initialize_timeline

    @constructor.generate_video
    after = @constructor.timeline.size
    after.should > Settings.min_final_size
  end


end
