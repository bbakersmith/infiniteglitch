require 'spec_helper'

describe Collector do


  def load_dummy_data(filename)
    path = "#{File.dirname(__FILE__)}/data/#{filename}"
    contents = JSON.parse(open(path, 'r').read)
    return contents
  end


  before :all do
    module Collector
      public :generate_id, 
             :select_a_file, 
             :select_supported_files,
             :get_item_details,
             :item_details_url,
             :matching_items,
             :format_response,
             :query_url
    end

    Settings.set({
      :query_args => {
        :query => {:format => ['h.264', 'Ogg Video', 'Flash Video']},
        :response => {:output => 'json', :page => 1}
      }
    })
  end


  before :each do
    @collector = Collector
    @supported_formats = Settings.query_args['query']['format']
    @query_details = load_dummy_data('ia_query.json')
    @item_details = load_dummy_data('ia_details.json')
    @item_id = '1959CommercialForThe1960ChevroletCorvair'
  end


  # !!! These tests make network calls, enable when needed !!!
  #

    # it "should retrieve item details" do
    #   id = '1959CommercialForThe1960ChevroletCorvair'
    #   details = @collector.get_item_details(id)
    #   details["server"].should_not be_empty
    # end


    # it "should download a random file" do
    #   # calling module directly to avoid mocking, this is the full test
    #   file_path = Collector.download_random_file
    #   file_path.length.should > 0
    # end

  #
  # !!! End network tests. !!!


  it "should change the page and nothing else" do
    @collector.query_url.should_not == @collector.query_url(9)
    @collector.query_url(4).should include "page=4"
  end


  it "should format a response string" do
    Settings.set({
      :query_args => {
        :response => {
          :return_fields => ['one', 'two']
        }
      }
    })

    expected = 'fl[]=one&fl[]=two'
    result = @collector.format_response
    result.should include expected
  end


  it "should return a proper query url" do
    url = @collector.query_url
    ['?q=', 'format:(', '&output=json', '&page='].all? do |partial_url|
      url.should include partial_url
    end
  end


  it "should return matching items" do
    matching = @collector.matching_items(@query_details)
    matching.should == 1268425
  end


  it "should return a proper item details url" do
    url = @collector.item_details_url('test')
    expected = 'http://archive.org/details/test?output=json'
    url.should == expected
  end


  it "should not return the item details" do
    details = @collector.get_item_details('fail')
    details.should be_false
  end


  it "should return videos" do
    files = @item_details["files"]
    videos = @collector.select_supported_files(files, @supported_formats)
    videos.values.first["size"].to_i.should > 0
  end


  it "should return a subset of the total" do
    # this test relies on there being at least one file that is not supported
    files = @item_details["files"]
    videos = @collector.select_supported_files(files, @supported_formats)
    videos.length.should < files.length
  end


  it "should select a video that's supported" do
    files = @item_details["files"]
    videos = @collector.select_supported_files(files, @supported_formats)
    # specific to current details test file
    videos.values.first["format"].should == "h.264"
  end


  it "should select the smallest supported video" do
    video_location = @collector.select_a_file(@item_details, @supported_formats)
    video_location.should == "/ChevroletCorvair1960ad1959.ogv"
  end


  it "should generate a unique name" do
    firstname = @collector.generate_id
    lastname = @collector.generate_id
    firstname.should_not == lastname
  end


end
