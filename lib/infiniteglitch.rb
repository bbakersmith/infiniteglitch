require "bundler"
Bundler.require(:default, :development)

libdir = File.dirname(__FILE__);

require libdir + "/infiniteglitch/settings"
InfiniteGlitch::Settings.load!(libdir + "/../config/settings.yml")

require libdir + "/infiniteglitch/archivist"
require libdir + "/infiniteglitch/filehandler"
require libdir + "/infiniteglitch/transcoder"
require libdir + "/infiniteglitch/collector"
require libdir + "/infiniteglitch/constructor"
require libdir + "/infiniteglitch/controller"

require libdir + "/aviglitch/frames"
