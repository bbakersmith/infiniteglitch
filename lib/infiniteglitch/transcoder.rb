require 'securerandom'

module InfiniteGlitch

  module Transcoder

    extend self


    def transcode(filename)
      input = "#{Settings.location('tmp')}/#{filename}"
      output = "#{Settings.location('tmp')}/#{filename}.#{format}"
      cmd = generate_cmd(input, output)

      puts "|| INPUT: #{input} ||"
      puts "|| OUTPUT: #{output} ||"
      puts "|| PARAMS: #{@parameters} ||"
      puts "|| CMD: #{cmd} ||"

      result = run_cmd(cmd)

      FileHandler.delete_file(input) if result != nil

      return output
    end


    private


    def generate_cmd(input, output)
      cmd =  "ffmpeg -y -i #{input} #{@parameters} #{output}"
      return cmd
    end


    def run_cmd(cmd)
      # TODO use beanstalk scheduler instead
      return system cmd rescue nil
    end


    def format
      Settings.send("#{@type}_format".to_sym)
    end


  end


  module SourceTranscoder

    include Transcoder

    extend self

    @type = 'source'
    @parameters = "-vcodec mpeg4 -vtag xvid " +
                  "-acodec ac3 -s 640x480 -aspect 4:3 -b 800k " +
                  "-g 250 -t 00:01:00"

  end


  module FinalTranscoder

    include Transcoder

    extend self

    @type = 'final'

    # TODO
    # would like to encode audio in future, tried but failing:
    # "-c:a libvorbis -b:a 192k -g 30 -s 640x480 -aspect 4:3 " +

    @parameters = "-vcodec libtheora " +
                  "-s 640x480 " +
                  "-r 20 -b 1600k"

  end

end
