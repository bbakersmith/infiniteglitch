#!/usr/bin/ruby
require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'net/http'


module InfiniteGlitch

  module Monitor


    def disk_space
      # return available disc space in megabytes
      current = `df -Pk #{Settings.loc_root} | grep ^/ | awk '{print $4;}'`.to_i / 1024 rescue nil
      return current > Settings.min_space
    end


    def directory_size(dir="/")
      # return size of dir in megabytes
      current = `du -Pk #{dir} | awk '{print $1;}'`.to_i / 1024 rescue nil
      return current
    end


    def check_stream
      place = URI.parse("http://infiniteglitch.com:8000/admin/stats.xml")
      connection = Net::HTTP.new(place.host, place.port)
      connection.start do |http|
        req = Net::HTTP::Get.new("#{place.path}?#{place.query}")
        req.basic_auth 'ben','Gl!tchB0t'
        @data = http.request(req).body
      end
      doc = Nokogiri::HTML(@data)
      res = doc.xpath('//source[@mount="/audio.ogg"]')
      res2 = doc.xpath('//source[@mount="/video.ogv"]')
      if res.size != 0 && res2.size != 0
        return true
      else
        return false
      end
    end


    def clean_tmp
      tempdirs = ["#{Settings.loc_vin}","#{Settings.loc_tmp}"]
      tempdirs.each do |d|
        Dir.foreach(d) do |f|
          cmd = "rm #{d}#{f}"
          if (Time.now.to_i - File.mtime("#{d}#{f}").to_i) > 1800 && f != '.' && f != '..'
          system cmd rescue nil
          end
        end
      end
    end


  end

end
