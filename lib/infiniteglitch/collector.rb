require 'open-uri'
require 'json'

module InfiniteGlitch

  module Collector 

    extend self


    def download_random_source
      begin
        # get count and derive random number from it
        no_list_details = get_response(query_url)
        random_page_num = rand(matching_items(no_list_details))
        # puts "no_list_details: #{no_list_details}"

        # get a random item
        list_details = get_response(query_url(random_page_num))
        item_id = list_details["response"]["docs"].first["identifier"]

        # get the item's details
        details = get_item_details(item_id)

        # select the smallest compatible file and form url
        filename = select_a_file(details, query_args['query']['format'])
        file_url = "http://#{details['server']}#{details['dir']}#{filename}"

        # download file
        download_result = download_file(file_url, filename)

        if download_result
          new_filename = SourceTranscoder.transcode(filename)

          source_id = generate_id
          SourceFileHandler.retrieve_from_tmp(new_filename, source_id)

          metadata = Hash[details["metadata"].map {|k,v| [k, v.first]}]
          source_record = metadata.merge({:id => source_id})
          Archivist.create_source(source_record)
          return source_id
        end

      rescue Exception => e
        puts "failed to complete source download: #{e}"
      end

      return nil
    end


    private


    def query_args
      # puts "||||||||||||||||||||||||||||||||||||||"
      settings_args = Settings.query_args
      # puts "settings_args: #{settings_args}"

      settings_args['response'].merge!({
        'sort_by' => ['headerImage asc'],
        'output' => 'json',
        'rows' => 1
      })

      return settings_args
    end


    def query_url(page = 0)
      query_string = "#{format_query}&#{format_response}&page=#{page}"
      full_url = "http://archive.org/advancedsearch.php?" + query_string
      # puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      encoded_url = URI::encode(full_url)
      # puts "query_url #{encoded_url}"
      return encoded_url
    end


    def get_response(url)
      begin
        details = JSON.parse(open(url).read)
      rescue Exception => e
        raise "failed to get a response: #{e}"
      end
    end


    def matching_items(details)
      return details["response"]["numFound"].to_i
    end


    def format_multiple_terms(key, terms)
      individual_strings = terms.map {|term| "#{key}=#{term}"}
      return individual_strings.join("&")
    end


    def format_response
      response_strings = query_args['response'].map do |k,v|
        if k == 'return_fields'
          format_multiple_terms("fl[]", v)
        elsif k == 'sort_by'
          format_multiple_terms("sort[]", v)
        else
          "#{k}=#{v}"
        end
      end
      return response_strings.join("&")
    end


    def format_query
      query = query_args['query']
      qval = query['q'] ? "#{query['q']} AND " : ''

      filtered_query = query.reject {|x| x == 'q'}

      partial_queries = filtered_query.map do |field,terms|
        encoded_options = terms.join(" OR ")
        encoded_partial = "#{field.to_s}:(#{encoded_options})"
      end

      combined_query = partial_queries.join(" AND ")
      query_with_var = "q=#{qval}#{combined_query}"

      # puts "qval: #{qval}"
      # puts "combined_query: #{combined_query}"
      # puts "query_with_var: #{query_with_var}"
      return query_with_var
    end


    def item_details_url(id)
      return "http://archive.org/details/#{id}?output=json"
    end


    def get_item_details(id)
      details_json = get_response(item_details_url(id))
      if details_json
        details_with_id = details_json.merge({"id" => id})
        return details_with_id
      else
        return false
      end
    end


    def select_supported_files(files, supported_formats)
      supported_files = files.select do |location, info| 
        file_format = info["format"]
        supported_formats.include?(info["format"])
      end
    end


    def sort_files_by_size(files)
      return files.sort_by {|k,v| v["size"].to_i}
    end


    def get_size(file)
      return file[1]["size"].to_i
    end


    def select_smallest_file(files)
      smallest = sort_files_by_size(files).first
      if smallest && get_size(smallest) <= Settings.max_download_size
        file_location = smallest.first
        return file_location
      else
        raise "no compatible source files within size limitations"
      end
    end


    def select_a_file(details, supported_formats)
      files = details["files"]
      if supported_formats
        supported_files = select_supported_files(files, supported_formats)
      else
        supported_files = files
      end
      selected_filename = select_smallest_file(supported_files)
      return selected_filename
    end


    def download_file(url, filename)
      begin
        file_location = "#{Settings.location('tmp')}#{filename}"
        Net::HTTP.get_response URI.parse(url) do |response|
          raise "Bad response: #{response.code}" if response.code != '200'
          open(file_location, "wb") do |file|
            file.write(response.read_body)
          end
        end
        return file_location
      rescue Exception => e
        puts "#{e.message}"
        puts "COLLECTOR: DOWNLOAD: START: FAILED"
        return false
      end
    end


    def generate_id
      return SecureRandom.uuid.gsub("-", "")
    end


  end

end
