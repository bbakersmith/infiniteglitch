require 'securerandom'

module InfiniteGlitch

  class Constructor


    def initialize
      @sources = []
      @segments = {
        :slides => []
      }
      # @timeline 
      @id = generate_id
    end


    def self.run
      Constructor.new.construct
    end


    def construct
      load_source_videos
      generate_slide_segments
      initialize_timeline
      generate_video
      finalize_video
    end


    private


    def load_source_videos
      target_dir = Settings.location('source')
      accepted_format = Settings.source_format
      sources = Archivist.get_random_sources(Settings.sources_per_final)

      sources.each do |source|
        filename = "#{target_dir}/#{source["id"]}.#{accepted_format}"
        load_video(filename)
      end

      Archivist.create_final({
        :id => @id,
        :sources => sources
      })
    end


    def load_video(filename)
      clip = AviGlitch.open(filename)
      pframes = []
      keys = []
      clip.frames.each_with_index do |f, i|
        if f.is_pframe?
          pframes.push(i)
        else
          keys.push(i)
        end
      end
      source_record = {:pframes => pframes, :keys => keys, :clip => clip}
      @sources.push(source_record)
      return @sources
    end


    def generate_slide_segments
      @segments[:slides] = []
      Settings.slides_per_final.times do
        source = @sources.sample
        reps = rand(Settings.max_slide_size)
        chosen_frame = source[:clip].frames[source[:pframes].sample, 1]

        # dupe to allow concat
        combined_frames = chosen_frame
        combined_frames.concat(chosen_frame * (reps -1))

        puts "CONSTRUCTOR: INIT_SLIDES: REPS: #{reps}"
        @segments[:slides].push(combined_frames)
      end
      return true
    end


    def initialize_timeline
      add_key
    end


    def generate_video
      if Settings.score && Settings.score.length > 0

        joined_score = Settings.score.join
        lookup = Settings.technique_shorthand
        joined_score.split(//).each do |shorthand|
          technique = lookup[shorthand]
          puts "CONSTRUCT: RUN SCORE: TECHNIQUE: #{technique.to_s}"
          self.send(technique) if technique != nil
        end

      else

        # construct starting with keyframe
        techniques = Settings.randomized_techniques
        while @timeline.size < Settings.min_final_size do
          technique = techniques.sample
          puts "CONSTRUCT: RUN RANDOM: TECHNIQUE: #{technique.to_s}"
          self.send(technique)
        end

      end
    end


    def finalize_video
      filename = "#{SecureRandom.uuid.gsub("-", "")}.avi"
      dir = Settings.location('tmp')
      AviGlitch.open(@timeline).output("#{dir}/#{filename}")
      new_filename = FinalTranscoder.transcode(filename)
      return FinalFileHandler.retrieve_from_tmp(new_filename, @id)
    end


    def generate_id
      return SecureRandom.uuid.gsub("-", "")
    end


    #################################################
    # construction_techniques
    ### all construction techniques modify @timeline
    #################################################


    def add_key
      source = @sources.sample
      key_index = source[:keys].sample
      chosen_frame = source[:clip].frames[key_index, 1]
      if @timeline
        @timeline.concat(chosen_frame)
      else
        @timeline = chosen_frame
      end
    end


    def add_segment
      num = rand(@sources.size)
      clip = @sources[num][:clip]
      start = rand(clip.frames.size)
      stop = rand(Settings.max_segment_size)
      stop = rand(clip.frames.size - start) if stop > (clip.frames.size - start)
      y = clip.frames[start, stop]
      @timeline.concat(y)
    end


    def slip
      num = rand(@sources.size)
      clip = @sources[num][:clip]
      pframes = @sources[num][:pframes]
      start = rand(pframes.size)
      stop = rand(Settings.max_segment_size)
      stop = rand(pframes.size - start) if stop > (pframes.size - start)
      x = clip.frames[pframes[start], stop]
      y = x.to_avi
      y.glitch :keyframe do |k|
        nil
      end
      @timeline.concat(y.frames)
    end

   
    def slide
      @timeline.concat(@segments[:slides].sample)
    end


  end

end
