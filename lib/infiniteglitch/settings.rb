require 'yaml'

module InfiniteGlitch

  module Settings

    extend self


    @_settings = {}
    ROOT_DIR = "#{File.dirname(__FILE__)}/../../"


    def load!(filename)
      @_settings = YAML::load_file(filename)
    end


    def set(new_settings)
      @_settings = deep_merge(@_settings, new_settings)
    end


    def location(location_name)
      return ROOT_DIR + @_settings['locations'][location_name]
    end
    

    def method_missing(name, *args, &block)
      return @_settings[name.to_s] || false
    end


    private


    def deep_merge(settings, new_settings)
      new_settings.each do |k, v|
        k = k.to_s
        if settings.has_key?(k)
          if v.class == Hash
            deep_merge(settings[k], v)
          else
            settings[k] = v
          end
        end
      end
      return settings
    end


  end

end
