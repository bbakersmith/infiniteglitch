module InfiniteGlitch

  module FileHandler

    extend self

    @loc = ''


    def all_matching_files
      directory = Settings.location(@loc)
      format = get_loc_format
      matching_files = Dir.glob("#{directory}/*.#{format}")
      return matching_files
    end


    def all_matching_file_ids
      return all_matching_files.map {|f| get_id_from_filename(f)}
    end


    def empty_pool
      matching_ids = all_matching_file_ids
      deleted_ids = matching_ids.map {|id| delete_file_by_id(id)}
      return deleted_ids
    end


    def remove_excess_from_pool
      deleted_ids = []
      diff = real_count_compared_to_max
      if diff > 0
        sorted_files = all_matching_files.sort_by {|f| File.mtime(f)}
        diff.times do
          selected_file = sorted_files.shift
          deleted_ids.push(delete_file(selected_file))
        end
      end
      return deleted_ids
    end


    def get_file_by_id(id)
      dir = Settings.location(@loc)
      format = get_loc_format
      matching_files = Dir.glob("#{dir}/#{id}.#{format}")
      if matching_files.length == 1
        return matching_files.first
      elsif matching_files.length == 0
        return nil
      else
        raise "More than one file found with given id: #{id}"
      end
    end


    def delete_file_by_id(id)
      file = get_file_by_id(id)
      result = delete_file(file)
      return result > 0 ? id : nil
    end


    def real_count_compared_to_max
      max = Settings.send("#{@loc}_max".to_sym)
      diff = all_matching_files.count - max
      return diff
    end


    def delete_file(file)
      return File.delete(file)
    end


    def retrieve_from_tmp(file_path, file_id)
      new_path = "#{Settings.location(@loc)}/#{file_id}.#{get_loc_format}"
      File.rename(file_path, new_path)
      return new_path
    end


    private


    def get_loc_format
      return Settings.send("#{@loc}_format".to_sym)
    end


    def get_id_from_filename(filename)
      match_data = filename.match(/([^\/]*)\.#{get_loc_format}/)
      if match_data && match_data.length > 1
        id = match_data[1]
        return id
      else
        raise "No valid id found"
      end
    end


  end


  module SourceFileHandler 

    include FileHandler

    extend self

    @loc = 'source'

  end


  module FinalFileHandler

    include FileHandler

    extend self

    @loc = 'final'


    def minimum_sources_present?
      min_sources = Settings.sources_per_final
      source_files = SourceFileHandler.all_matching_files
      return source_files.length >= min_sources
    end


  end

end
