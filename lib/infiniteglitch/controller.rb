module InfiniteGlitch

  module Controller

    extend self


    def start
      scheduler = Rufus::Scheduler.new

      scheduler.every Settings.collection_rate do
        puts "Download begun: #{Time.now.to_s}"
        Collector.download_random_file
        puts "Download complete: #{Time.now.to_s}"
      end

      scheduler.every Settings.construction_rate do
        puts "Construction begun: #{Time.now.to_s}"
        Constructor.run
        puts "Construction complete: #{Time.now.to_s}"
      end

      scheduler.join
    end


    def stop
      # stop all infinite glitch processes
      #
      # clear tmp dir
    end


    def fill_source_pool
      # download source videos until source limit is reached
      diff = SourceFileHandler.real_count_compared_to_max
      if diff < 0
        collect(diff.abs)
      elsif diff > 0
        SourceFileHandler.remove_excess_from_pool
      end
    end


    def fill_final_pool
      # generate final videos until final limit is reached
      diff = FinalFileHandler.real_count_compared_to_max
      if diff < 0 
        if FinalFileHandler.minimum_sources_present?
          construct(diff.abs)
        else
          raise "Cannot construct without necessary source files"
        end
      elsif diff > 0
        FinalFileHandler.remove_excess_from_pool
      end
    end


    def empty_source_pool
      deleted_ids = SourceFileHandler.empty_pool
      deleted_ids.each {|id| Archivist.delete_source(id)}
      return deleted_ids.length
    end


    def empty_final_pool
      deleted_ids = FinalFileHandler.empty_pool
      deleted_ids.each {|id| Archivist.delete_final(id)}
      return deleted_ids.length
    end


    def clean_source_pool
      known_records = Archivist.get_all_source_ids
      known_files = SourceFileHandler.all_matching_file_ids
      confirmed_ids = known_records.select {|id| known_files.include? id}

      orphan_records = known_records.reject {|id| confirmed_ids.include?(id)}
      oprhan_files = known_files.reject {|id| confirmed_ids.include?(id)}

      orphan_records.each {|id| Archivist.delete_source(id)}
      oprhan_files.each {|id| SourceFileHandler.delete_file_by_id(id)}

      return confirmed_ids
    end


    def clean_final_pool
      known_records = Archivist.get_all_final_ids
      known_files = FinalFileHandler.all_matching_file_ids
      confirmed_ids = known_records.select {|id| known_files.include? id}

      orphan_records = known_records.reject {|id| confirmed_ids.include?(id)}
      orphan_files = known_files.reject {|id| confirmed_ids.include?(id)}

      orphan_records.each {|id| Archivist.delete_final(id)}
      orphan_files.each {|id| FinalFileHandler.delete_file_by_id(id)}

      return confirmed_ids
    end


    def collect(num = 1)
      ids = repeat_with_failure_limit(num) {Collector.download_random_source}
      return ids
    end


    def construct(num = 1)
      ids = repeat_with_failure_limit(num) {Constructor.run}
      return ids
    end


    def next_video
      selected_final_id = Archivist.get_random_final(1).first['id']
      selected_file = FinalFileHandler.get_file_by_id(selected_final_id)
      Archivist.update_current(selected_final_id)
      return selected_file
    end


    def current_video
      final_id = Archivist.get_current_id
      file_path = FinalFileHandler.get_file_by_id(final_id)
      return file_path
    end


    def current_video_metadata
      final_id = Archivist.get_current_id
      return final_video_metadata(final_id)
    end


    def fuzzy_video_metadata(partial_id)
      known_finals = Archivist.get_all_final_ids
      matching_finals = known_finals.select do |id| 
        id.match(/^#{partial_id}/)
      end

      if matching_finals.length == 1
        return final_video_metadata(matching_finals[0])      
      elsif matching_finals.length > 1
        raise "Too many matching final records found."
      end

      known_sources = Archivist.get_all_source_ids
      matching_sources = known_sources.select do |id| 
        id.match(/^#{partial_id}/)
      end

      if matching_sources.length == 1
        return source_video_metadata(matching_sources[0])
      elsif matching_sources.length > 1
        raise "Too many matching source records found."
      end

      raise "No matching records found."
    end


    def source_video_metadata(source_id)
      metadata = Archivist.get_source(source_id)
      return metadata
    end


    def final_video_metadata(final_id)
      metadata = Archivist.get_final(final_id)
      return metadata
    end


    private


    def repeat_with_failure_limit(num)
      ids = []
      consecutive_failures = 0

      until ids.length >= num

        id = yield

        if id
          ids.push(id)
          consecutive_failures = 0
        else
          consecutive_failures += 1
        end

        if consecutive_failures >= Settings.max_consecutive_failures
          raise "Process stopped because of too many consecutive failures: #{consecutive_failures}"
        end
      end

      return ids
    end


  end

end
