module InfiniteGlitch

  module Archivist

    include RethinkDB::Shortcuts

    extend self


    CONNECTION = r.connect({:host => "localhost", :port => 28015})
    DATABASE_NAME = Settings.database
    TABLES = ['source', 'final', 'current']
    CURRENT_ID = 1


    def initialize_database
      create_database
      create_tables
    end


    def database
      return r.db(DATABASE_NAME)
    end


    def table(name)
      return database.table(name)
    end


    def delete_database
      begin
        r.db_drop(DATABASE_NAME).run(CONNECTION)
        puts "deleted database '#{DATABASE_NAME}'"
        return true
      rescue => e
        puts "failed to delete database: #{e}"
        return false
      end
    end


    def clear_tables
      TABLES.each {|t| table(t).delete.run(CONNECTION)}
    end


    def create_source(metadata)
      id = insert_to_table('source', metadata)
      return id
    end


    def get_source(source_id)
      return table('source').get(source_id).run(CONNECTION)
    end


    def get_random_sources(num)
      get_random_records('source', num)
    end


    def get_all_source_ids
      return get_all_ids('source')
    end

    
    def delete_source(source_id)
      return table('source').get(source_id).delete.run(CONNECTION)["deleted"]
    end


    def create_final(metadata)
      id = insert_to_table('final', metadata)
      return id
    end


    def get_final(final_id)
      return table('final').get(final_id).run(CONNECTION)
    end


    def get_random_final(num)
      return get_random_records('final', num)
    end


    def get_all_final_ids
      return get_all_ids('final')
    end


    def delete_final(final_id)
      if is_current?(final_id)
        return 0
      else
        return table('final').get(final_id).delete.run(CONNECTION)["deleted"]
      end
    end


    def force_delete_final(final_id)
      delete_current if is_current?(final_id)
      return table('final').get(final_id).delete.run(CONNECTION)["deleted"]
    end


    def get_current_id
      current_record = table('current').get(CURRENT_ID).run(CONNECTION)
      return current_record ? current_record['final_id'] : nil
    end


    def update_current(final_id)
      # there is only one row in the current table;
      # this could be modified to accommodate a recent history
      metadata = {
        :id => CURRENT_ID,
        :final_id => final_id
      }
      begin
        result = insert_to_table('current', metadata)
      rescue => e
        puts "failed to update 'current' table: #{e}"
      end

      # also write to file if specified in config
      metalocation = Settings.locations['current_metadata']
      if metalocation
        IO.write(metalocation, get_final(final_id).to_json)
      end

      return result
    end


    private


    def create_database
      begin
        r.db_create(DATABASE_NAME).run(CONNECTION)
        puts "created database '#{DATABASE_NAME}'"
        return true
      rescue => e
        puts "failed to create database: #{e}"
        return false
      end
    end


    def create_tables
      tables_created = 0
      TABLES.each do |table| 
        begin
          database.table_create(table).run(CONNECTION)
          puts "created table '#{table}'"
          tables_created += 1
        rescue => e
          puts "failed to create table: #{e}"
        end
      end
      return tables_created
    end


    def get_random_records(table, num)
      selected = table(table).sample(num).coerce_to('array').run(CONNECTION)
      if selected.length > 0
        return selected
      else
        raise "No record found."
      end
    end


    def delete_current
      res = table('current').get(CURRENT_ID).delete.run(CONNECTION)["deleted"]
      return res
    end


    def is_current?(final_id)
      return final_id == get_current_id
    end


    def insert_to_table(table_name, row)
      row[:created_at] = Time.now
      # overwrite if record exists
      options = {:conflict => "replace"} 
      result = table(table_name).insert(row, options).run(CONNECTION)

      if row[:id]
        return row[:id]
      elsif result["generated_keys"].length == 1
        return result["generated_keys"].first
      else
        raise "error setting source metadata: #{result}"
      end
    end


    def get_all_ids(table_name)
      return table(table_name).run(CONNECTION).map {|x| x['id']}
    end


  end

end
