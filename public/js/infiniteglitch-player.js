var IG = {

  lastLoop: 0,

  videoContainer: function() {
    return document.getElementById('videoContainer');
  },

  videoPlayer: function() {
    return document.getElementById('infiniteStream');
  },

  currentTime: function() {
    return IG.videoPlayer().currentTime;
  },

  destroyVideoPlayer: function() {
    IG.videoContainer().removeChild(IG.videoPlayer());
  },

  createVideoPlayer: function() {
    var newPlayer = document.createElement('video');
    newPlayer.id = 'infiniteStream';
    newPlayer.src = 'http://infiniteglitch.com:8001/video.ogv';
    newPlayer.autoplay = 'autoplay';
    newPlayer.autobuffer = 'autobuffer';
    newPlayer.loop = 'loop';
    return IG.videoContainer().appendChild(newPlayer);
  },

  reload: function() {
    console.log("reloading...");
    IG.destroyVideoPlayer();
    IG.createVideoPlayer();
  },

  loop: function() {
    // store interval object to facilitate clearing 
    IG.loopInterval = window.setInterval(function() {
      var now = IG.currentTime();
      console.log(now);
      // current time should be more than 0 and less than 1 year
      if(now === IG.lastLoop || now < 0 || now > 31536000) {
        IG.lastLoop = 0;
        IG.reload();
      } else {
        IG.lastLoop = now;
      }
    }, 2000);
  },

  // public

  play: function() {
    IG.createVideoPlayer();
    IG.loop();
  },

  stop: function() {
    window.clearInterval(IG.loopInterval);
    IG.destroyVideoPlayer();
  }

};

IG.play(); // run
